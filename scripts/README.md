**Scripts:**<br>
[adjust git20150322](https://github.com/dubhater/vapoursynth-adjust)<br>
[havsfunc r23](https://github.com/HomeOfVapourSynthEvolution/havsfunc)<br>
[maskdetail git20151220](https://github.com/MonoS/VS-MaskDetail)<br>
[mvsfunc r8](https://github.com/HomeOfVapourSynthEvolution/mvsfunc)<br>
[vapoursynth-modules git20131225](https://github.com/4re/vapoursynth-modules)<br>
[vsTAAmbk 0.4.1](https://github.com/HomeOfVapourSynthEvolution/vsTAAmbk)<br>

dehalo_alpha, edgecleaner, fastlinedarken, finesharp, knlm, mcdegrainsharp, nnedi3_rpow2,
psharpen, resamplehq, sharpaamcmod, supersampledantialiasing, vshelpers: https://gist.github.com/4re/
